// Copyright (c) 2017 Dell Boomi, Inc.

var httpProxy = require('http-proxy');
var btoa = require('btoa');
var https = require('https');
var url = require('url');

function createProxy(options) {
	var parsedUrl = url.parse(options.baseUrl);
	var proxy = httpProxy.createProxyServer({
		target: options.baseUrl,
		agent  : https.globalAgent,
		headers: {
			host: parsedUrl.hostname,
			Authorization: "Basic " + btoa(options.userid+":"+options.password)
		}	
	});
	if (options.proxyPort) {
		proxy.listen(options.proxyPort);
	}	
	return proxy;
}

exports.createProxy = createProxy;