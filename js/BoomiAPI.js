// Copyright (c) 2015 Dell Boomi, Inc.

(function(factory) {
	var root = (typeof self == 'object' && self.self == self && self) ||
      		   (typeof global == 'object' && global.global == global && global);
	// AMD
	if (typeof define === "function" && define.amd) {
		define(["jquery", "q", "exports"], function($, Q, exports) {
			root.BoomiAPI = factory(root, exports, $.ajax, Q, root.btoa);
		});
	// Node.js or CommonJS
	} else if (typeof exports !== 'undefined') {
        var Q = require('q');
        var bootstrap = require('./bootstrap');
		factory(root, exports, bootstrap.ajax, Q, bootstrap.btoa, bootstrap.BoomiAPIProxy);
	// Browser global
	} else {
		var $ = root.jQuery || root.$;
		root.BoomiAPI = factory(root, {}, $.ajax, root.Q, root.btoa);
	}
}(function(root, BoomiAPI, ajax, Q, btoa, BoomiAPIProxy) {
	function callApi(options, params) {
		var url = options.baseUrl;
		if (params.overrideAccountId || params.partner) {
			url += "/partner";
		}
		url += "/api/rest/v1/"+options.accountId+"/"+options.object;
		if (params.filter) {
			url += "/query";
		} else if (params.queryToken) {
			url += "/queryMore";
		} else if (params.execute) {
			url += "/execute";
			if (params.id) {
				url += "/"+params.id;
			}
		} else if (params.bulk) {
			url += "/bulk";
		} else if (params.id) {
			url += "/"+params.id;
			if (params.data) {
				url += "/update";
			}
		}
		if (params.additional) {
			url += params.additional;
		}
		if (params.overrideAccountId) {
			url += "?overrideAccount="+params.overrideAccountId
		}

		var deferred = Q.defer();

		var ajaxOptions = {
			url: url,
			type: params.type,
            contentType: "application/json",
            rejectUnauthorized: false,
            crossDomain: true,
            xhrFields: {
            	withCredentials: true
            },
            success: function(data, textStatus, jqXHR) {
            	if (options.debug && this.type !== "DELETE") {
            		console.log("debug: response "+JSON.stringify(data));
            	}
            	params.success(data, deferred);
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	if (jqXHR.responseJSON) {
					if (options.debug) {
						console.log("debug: error: ["+JSON.stringify(jqXHR.responseJSON)+"]");
					}
	            	deferred.reject(jqXHR.responseJSON);
	        	} else {
					if (options.debug) {
						console.log("debug: error: ["+jqXHR.responseText+"]");
					}
	        		deferred.reject({message: jqXHR.responseText, status: jqXHR.status, responseText: jqXHR.responseText});
	        	}
	        }
		};

		if (params.data) {
			ajaxOptions.data = JSON.stringify(params.data);
		} else if (params.filter) {
			ajaxOptions.data = JSON.stringify(params.filter);
		} else if (params.bulk) {
			ajaxOptions.data = JSON.stringify(params.bulk);
		} else if (params.queryToken) {
			ajaxOptions.data = params.queryToken;
			ajaxOptions.contentType = "text/plain";
		} else if (params.execute) {
			ajaxOptions.data = JSON.stringify(params.execute);
		}

		ajaxOptions.headers = {
			"Content-Type": "application/json"
		};

		if (!options.isProxy) {
			ajaxOptions.headers["Authorization"] = "Basic " + btoa(options.userid+":"+options.password);
		}

		if (ajaxOptions.type !== "DELETE" && !params.noResponse) {
			ajaxOptions.headers.Accept = "application/json";
			ajaxOptions.dataType = "json";
		}

		if (options.debug) {
			console.log("debug: type: ["+ajaxOptions.type+"] url: ["+url+"]");
			if (ajaxOptions.data) {
				console.log("debug: data in: ["+ajaxOptions.data+"]");
			}
		}
		ajax(ajaxOptions);

		return deferred.promise;
	}

	function get(options, params, propertyName, additional) {
		if (!params.id) {
			throw new Error("Get request must provide an id parameter");
		}

		return callApi(options, {
			id: params.id,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "GET",
            additional: additional,
            success: function(result, deferred) {
            	params[propertyName] = result;
                deferred.resolve(params);
			}
		});
	}

	function query(options, params, propertyName, additional) {
		if (params.filter || params.queryToken) {
			return callApi(options, {
				filter: params.filter,
				queryToken: params.queryToken,
				overrideAccountId: params.overrideAccountId,
				partner: params.partner,
	            type: "POST",
            	additional: additional,
	            success: function(result, deferred) {
	            	params[propertyName] = result;
	                deferred.resolve(params);
				}
			});
		} else {
			throw new Error("Query request must provide a filter or a queryToken parameter");
		}
	}

	function create(options, params, propertyName, additional) {
		if (!params.data) {
			throw new Error("Create request must provide an data parameter");
		}

		return callApi(options, {
			data: params.data,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "POST",
            additional: additional,
            success: function(result, deferred) {
            	params[propertyName] = result;
                deferred.resolve(params);
			}
		});
	}

	function update(options, params, propertyName, additional) {
		if (!params.id) {
			throw new Error("Update request must provide an id parameter");
		}
		if (!params.data) {
			throw new Error("Update request must provide an data parameter");
		}

		return callApi(options, {
			id: params.id,
			data: params.data,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "POST",
            additional: additional,
            success: function(result, deferred) {
            	params[propertyName] = result;
                deferred.resolve(params);
			}
		});
	}

	function execute(options, params, propertyName, additional) {
	/*
		if (!params.id) {
			throw new Error("Execute request must provide an id parameter");
		}
	*/
		if (!params.data) {
			throw new Error("Execute request must provide an data parameter");
		}
		return callApi(options, {
			id: params.id,
			execute: params.data,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "POST",
            additional: additional,
            success: function(result, deferred) {
            	params[propertyName] = result;
                deferred.resolve(params);
			}
		});
	}

	function del(options, params, additional) {
		if (!params.id) {
			throw new Error("Delete request must provide an id parameter");
		}

		return callApi(options, {
			id: params.id,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "DELETE",
            additional: additional,
            success: function(result, deferred) {
                deferred.resolve(params);
			}
		});
	}

	function bulk(options, params, propertyName, additional) {
		if (!params.bulk) {
			throw new Error("Bulk request must provide an bulk parameter");
		}

		return callApi(options, {
			bulk: params.bulk,
			overrideAccountId: params.overrideAccountId,
			partner: params.partner,
            type: "POST",
            additional: additional,
            success: function(result, deferred) {
            	params[propertyName] = result;
                deferred.resolve(params);
			}
		});
	}

	function copyOptions(options) {
		return JSON.parse(JSON.stringify(options));
	}

	var Account = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Account";
	};

	Account.prototype = {
		get: function(params) {
			return get(this.options, params, "account");
		},
		query: function(params) {
			return query(this.options, params, "accounts");
		},
		create: function(params) {
			return create(this.options, params, "account");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "accounts");
		}
	};

	var AccountGroupAccount = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountGroupAccount";
	}

	AccountGroupAccount.prototype = {
		query: function(params) {
			return query(this.options, params, "accountGroupAccounts");
		},
		create: function(params) {
			return create(this.options, params, "accountGroupAccount");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var AccountGroupUserRole = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountGroupUserRole";
	}

	AccountGroupUserRole.prototype = {
		query: function(params) {
			return query(this.options, params, "accountGroupUserRoles");
		},
		create: function(params) {
			return create(this.options, params, "accountGroupUserRole");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var AccountGroup = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountGroup";
	}

	AccountGroup.prototype = {
		get: function(params) {
			return get(this.options, params, "accountGroup");
		},
		query: function(params) {
			return query(this.options, params, "accountGroups");
		},
		create: function(params) {
			return create(this.options, params, "accountGroup");
		},
		update: function(params) {
			return update(this.options, params, "accountGroup");
		},
		bulk: function(params) {
			return bulk(this.options, params, "accountGroups");
		}
	};

	var AccountSSOConfig = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountSSOConfig";
	}

	AccountSSOConfig.prototype = {
		get: function(params) {
			return get(this.options, params, "accountSSOConfig");
		},
		update: function(params) {
			return update(this.options, params, "accountSSOConfig");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "accountSSOConfigs");
		}
	};

	var AccountUserFederation = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountUserFederation";
	}

	AccountUserFederation.prototype = {
		query: function(params) {
			return query(this.options, params, "accountUserFederations");
		},
		create: function(params) {
			return create(this.options, params, "accountUserFederation");
		},
		update: function(params) {
			return update(this.options, params, "accountUserFederation");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var AccountUserRole = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountUserRole";
	}

	AccountUserRole.prototype = {
		query: function(params) {
			return query(this.options, params, "accountUserRoles");
		},
		create: function(params) {
			return create(this.options, params, "accountUserRole");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var AtomConnectionFieldExtensionSummary = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomConnectionFieldExtensionSummary";
	}

	AtomConnectionFieldExtensionSummary.prototype = {
		query: function(params) {
			return query(this.options, params, "atomConnectionFieldExtensionSummaries");
		}
	};

	var AtomConnectorVersions = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomConnectionFieldExtensionSummary";
	}

	AtomConnectorVersions.prototype = {
		get: function(params) {
			return get(this.options, params, "atomConnectorVersions");
		},
		bulk: function(params) {
			return bulk(this.options, params, "atomConnectorVersions");
		}
	};

	var Environment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Environment";
	}

	Environment.prototype = {
		get: function(params) {
			return get(this.options, params, "environment");
		},
		query: function(params) {
			return query(this.options, params, "environments");
		},
		create: function(params) {
			return create(this.options, params, "environment");
		},
		update: function(params) {
			return update(this.options, params, "environment");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "environments");
		}
	};

	var IntegrationPack = function(options) {
		this.options = copyOptions(options);
		this.options.object = "IntegrationPack";
	}

	IntegrationPack.prototype = {
		get: function(params) {
			return get(this.options, params, "integrationPack");
		},
		query: function(params) {
			return query(this.options, params, "integrationPacks");
		},
		bulk: function(params) {
			return bulk(this.options, params, "integrationPacks");
		}
	};

	var IntegrationPackInstance = function(options) {
		this.options = copyOptions(options);
		this.options.object = "IntegrationPackInstance";
	}

	IntegrationPackInstance.prototype = {
		get: function(params) {
			return get(this.options, params, "integrationPackInstance");
		},
		query: function(params) {
			return query(this.options, params, "integrationPackInstances");
		},
		create: function(params) {
			return create(this.options, params, "integrationPackInstance");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "integrationPackInstances");
		}
	};

	var IntegrationPackEnvironmentAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "IntegrationPackEnvironmentAttachment";
	}

	IntegrationPackEnvironmentAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "integrationPackEnvironmentAttachments");
		},
		create: function(params) {
			return create(this.options, params, "integrationPackEnvironmentAttachment");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var Cloud = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Cloud";
	}

	Cloud.prototype = {
		get: function(params) {
			return get(this.options, params, "cloud");
		},
		query: function(params) {
			return query(this.options, params, "clouds");
		},
		bulk: function(params) {
			return bulk(this.options, params, "clouds");
		}
	};

	var Atom = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Atom";
	}

	Atom.prototype = {
		get: function(params) {
			return get(this.options, params, "atom");
		},
		query: function(params) {
			return query(this.options, params, "atoms");
		},
		create: function(params) {
			return create(this.options, params, "atom");
		},
		update: function(params) {
			return update(this.options, params, "atom");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "atoms");
		}
	};

	var EnvironmentAtomAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentAtomAttachment";
	}

	EnvironmentAtomAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "environmentAtomAttachments");
		},
		create: function(params) {
			return create(this.options, params, "environmentAtomAttachment");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var EnvironmentExtensions = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentExtensions";
	}

	EnvironmentExtensions.prototype = {
		get: function(params) {
			return get(this.options, params, "environmentExtensions");
		},
		query: function(params) {
			return query(this.options, params, "environmentExtensions");
		},
		update: function(params) {
			return update(this.options, params, "environmentExtensions");
		},
		bulk: function(params) {
			return bulk(this.options, params, "environmentExtensions");
		}
	};

	var AtomExtensions = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomExtensions";
	}

	AtomExtensions.prototype = {
		get: function(params) {
			return get(this.options, params, "atomExtensions");
		},
		query: function(params) {
			return query(this.options, params, "atomExtensions");
		},
		update: function(params) {
			return update(this.options, params, "atomExtensions");
		},
		bulk: function(params) {
			return bulk(this.options, params, "atomExtensions");
		}
	};

	var Process = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Process";
	}

	Process.prototype = {
		get: function(params) {
			return get(this.options, params, "process");
		},
		query: function(params) {
			return query(this.options, params, "processes");
		},
		bulk: function(params) {
			return bulk(this.options, params, "processes");
		}
	};

	var ExecutionRecord = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ExecutionRecord";
	}

	ExecutionRecord.prototype = {
		query: function(params) {
			return query(this.options, params, "executionRecords");
		}
	};

	var ProcessSchedules = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ProcessSchedules";
	}

	ProcessSchedules.prototype = {
		get: function(params) {
			return get(this.options, params, "processSchedules");
		},
		query: function(params) {
			return query(this.options, params, "processSchedules");
		},
		update: function(params) {
			return update(this.options, params, "processSchedules");
		},
		bulk: function(params) {
			return bulk(this.options, params, "processSchedules");
		}
	};

	var AccountUserRole = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountUserRole";
	}

	AccountUserRole.prototype = {
		query: function(params) {
			return query(this.options, params, "accountUserRoles");
		},
		create: function(params) {
			return create(this.options, params, "accountUserRole");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var EnvironmentMapExtensionsSummary = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentMapExtensionsSummary";
	}

	EnvironmentMapExtensionsSummary.prototype = {
		query: function(params) {
			return query(this.options, params, "environmentMapExtensionsSummaries");
		}
	};

	var AtomMapExtensionsSummary = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomMapExtensionsSummary";
	}

	AtomMapExtensionsSummary.prototype = {
		query: function(params) {
			return query(this.options, params, "atomMapExtensionsSummaries");
		}
	};

	var EnvironmentMapExtension = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentMapExtension";
	}

	EnvironmentMapExtension.prototype = {
		get: function(params) {
			return get(this.options, params, "environmentMapExtension");
		},
		update: function(params) {
			return update(this.options, params, "environmentMapExtension");
		},
		execute: function(params) {
			return execute(this.options, params, "environmentMapExtension");
		},
		bulk: function(params) {
			return bulk(this.options, params, "environmentMapExtensions");
		}
	};

	var AtomMapExtension = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomMapExtension";
	}

	AtomMapExtension.prototype = {
		get: function(params) {
			return get(this.options, params, "atomMapExtension");
		},
		update: function(params) {
			return update(this.options, params, "atomMapExtension");
		},
		execute: function(params) {
			return execute(this.options, params, "atomMapExtension");
		},
		bulk: function(params) {
			return bulk(this.options, params, "atomMapExtensions");
		}
	};

	var Event = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Event";
	}

	Event.prototype = {
		query: function(params) {
			return query(this.options, params, "events");
		}
	};

	var ExecutionCountAccount = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ExecutionCountAccount";
	}

	ExecutionCountAccount.prototype = {
		query: function(params) {
			return query(this.options, params, "executionCountAccounts");
		}
	};

	var ExecutionCountAccountGroup = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ExecutionCountAccountGroup";
	}

	ExecutionCountAccountGroup.prototype = {
		query: function(params) {
			return query(this.options, params, "executionCountAccountGroups");
		}
	};

	var ExecutionSummaryRecord = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ExecutionSummaryRecord";
	}

	ExecutionSummaryRecord.prototype = {
		query: function(params) {
			return query(this.options, params, "executionSummaryRecords");
		}
	};

	var IntegrationPackAtomAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "IntegrationPackAtomAttachment";
	}

	IntegrationPackAtomAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "integrationPackAtomAttachments");
		},
		create: function(params) {
			return create(this.options, params, "integrationPackAtomAttachment");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var AtomStartupProperties = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AtomStartupProperties";
	}

	AtomStartupProperties.prototype = {
		get: function(params) {
			return get(this.options, params, "atomStartupProperties");
		},
		execute: function(params) {
			return execute(this.options, params, "atomStartupProperties");
		},
		bulk: function(params) {
			return bulk(this.options, params, "atomStartupProperties");
		}
	};

	var AuditLog = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AuditLog";
	}

	AuditLog.prototype = {
		query: function(params) {
			return query(this.options, params, "auditLogs");
		}
	};

	var CustomTrackedField = function(options) {
		this.options = copyOptions(options);
		this.options.object = "CustomTrackedField";
	}

	CustomTrackedField.prototype = {
		query: function(params) {
			return query(this.options, params, "customTrackedFields");
		}
	};

	var Deployment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Deployment";
	}

	Deployment.prototype = {
		get: function(params) {
			return get(this.options, params, "deployment");
		},
		query: function(params) {
			return query(this.options, params, "deployments");
		},
		create: function(params) {
			return create(this.options, params, "deployment");
		},
		bulk: function(params) {
			return bulk(this.options, params, "deployments");
		}
	};

	var DocumentCountAccount = function(options) {
		this.options = copyOptions(options);
		this.options.object = "DocumentCountAccount";
	}

	DocumentCountAccount.prototype = {
		query: function(params) {
			return query(this.options, params, "documentCountAccounts");
		}
	};

	var DocumentCountAccountGroup = function(options) {
		this.options = copyOptions(options);
		this.options.object = "DocumentCountAccountGroup";
	}

	DocumentCountAccountGroup.prototype = {
		query: function(params) {
			return query(this.options, params, "documentCountAccountGroups");
		}
	};

	var EnvironmentConnectionFieldExtensionSummary = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentConnectionFieldExtensionSummary";
	}

	EnvironmentConnectionFieldExtensionSummary.prototype = {
		query: function(params) {
			return query(this.options, params, "environmentConnectionFieldExtensionSummaries");
		}
	};

	var ProcessAtomAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ProcessAtomAttachment";
	}

	ProcessAtomAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "processAtomAttachments");
		},
		create: function(params) {
			return create(this.options, params, "processAtomAttachment");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var ProcessEnvironmentAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ProcessEnvironmentAttachment";
	}

	ProcessEnvironmentAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "processEnvironmentAttachments");
		},
		create: function(params) {
			return create(this.options, params, "processEnvironmentAttachment");
		},
		del: function(params) {
			return del(this.options, params);
		}
	};

	var ProcessScheduleStatus = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ProcessScheduleStatus";
	}

	ProcessScheduleStatus.prototype = {
		get: function(params) {
			return get(this.options, params, "processScheduleStatus");
		},
		query: function(params) {
			return query(this.options, params, "processScheduleStatuses");
		},
		update: function(params) {
			return update(this.options, params, "processScheduleStatus");
		},
		bulk: function(params) {
			return bulk(this.options, params, "processScheduleStatuses");
		}
	};

	var Role = function(options) {
		this.options = copyOptions(options);
		this.options.object = "Role";
	}

	Role.prototype = {
		get: function(params) {
			return get(this.options, params, "role");
		},
		query: function(params) {
			return query(this.options, params, "roles");
		},
		update: function(params) {
			return update(this.options, params, "role");
		},
		create: function(params) {
			return create(this.options, params, "role");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "roles");
		}
	};

	var SharedServerInformation = function(options) {
		this.options = copyOptions(options);
		this.options.object = "SharedServerInformation";
	}

	SharedServerInformation.prototype = {
		get: function(params) {
			return get(this.options, params, "sharedServerInformation");
		},
		update: function(params) {
			return update(this.options, params, "sharedServerInformation");
		},
		bulk: function(params) {
			return bulk(this.options, params, "sharedServerInformations");
		}
	};

	var ThroughputAccount = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ThroughputAccount";
	}

	ThroughputAccount.prototype = {
		query: function(params) {
			return query(this.options, params, "throughputAccounts");
		}
	};

	var ThroughputAccountGroup = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ThroughputAccountGroup";
	}

	ThroughputAccountGroup.prototype = {
		query: function(params) {
			return query(this.options, params, "throughputAccountGroups");
		}
	};

	var WidgetInstanceExtensions = function(options) {
		this.options = copyOptions(options);
		this.options.object = "WidgetInstanceExtensions";
	}

	WidgetInstanceExtensions.prototype = {
		get: function(params) {
			return get(this.options, params, "widgetInstanceExtension");
		},
		update: function(params) {
			return update(this.options, params, "widgetInstanceExtensions");
		},
		bulk: function(params) {
			return bulk(this.options, params, "widgetInstanceExtensions");
		}
	};

	var X12ConnectorRecord = function(options) {
		this.options = copyOptions(options);
		this.options.object = "X12ConnectorRecord";
	}

	X12ConnectorRecord.prototype = {
		query: function(params) {
			return query(this.options, params, "x12ConnectorRecords");
		}
	};

	var AccountProvision = function(options) {
		this.options = copyOptions(options);
		this.options.object = "AccountProvision";
	}

	AccountProvision.prototype = {
		get: function(params) {
			params.partner = true;
			return get(this.options, params, "accountProvision");
		},
		execute: function(params) {
			params.partner = true;
			return execute(this.options, params, "accountProvision");
		}
	};

	var InstallerToken = function(options) {
		this.options = copyOptions(options);
		this.options.object = "InstallerToken";
	}

	InstallerToken.prototype = {
		create: function(params) {
			return create(this.options, params, "installerToken");
		}
	};

	var EnvironmentRole = function(options) {
		this.options = copyOptions(options);
		this.options.object = "EnvironmentRole";
	}

	EnvironmentRole.prototype = {
		get: function(params) {
			return get(this.options, params, "environmentRole");
		},
		query: function(params) {
			return query(this.options, params, "environmentRoles");
		},
		create: function(params) {
			return create(this.options, params, "environmentRole");
		},
		del: function(params) {
			return del(this.options, params);
		},
		bulk: function(params) {
			return bulk(this.options, params, "environmentRoles");
		}
	};

	var ComponentAtomAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ComponentAtomAttachment";
	}

	ComponentAtomAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "componentAtomAttachments");
		},
		create: function(params) {
			return create(this.options, params, "componentAtomAttachment");
		}
	};

	var ComponentEnvironmentAttachment = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ComponentEnvironmentAttachment";
	}

	ComponentEnvironmentAttachment.prototype = {
		query: function(params) {
			return query(this.options, params, "componentEnvironmentAttachments");
		},
		create: function(params) {
			return create(this.options, params, "componentEnvironmentAttachment");
		}
	};

	var ListenerStatus = function(options) {
		this.options = copyOptions(options);
		this.options.object = "ListenerStatus";
	}

	ListenerStatus.prototype = {
		get: function(params) {
			return callApi(this.options, {
				overrideAccountId: params.overrideAccountId,
				partner: params.partner,
				type: "GET",
				additional: "/async/"+params.id,
				success: function(result, deferred) {
					params["listenerStatus"] = result;
					deferred.resolve(params);
				}
			});
		},
		query: function(params) {
			return query(this.options, params, "listenerStatuses", "/async");
		}
	}

	var PackagedComponent = function(options) {
		this.options = copyOptions(options);
		this.options.object = "PackagedComponent";
	}

	PackagedComponent.prototype = {
		get: function(params) {
			return get(this.options, params, "packagedComponent");
		},
		query: function(params) {
			return query(this.options, params, "packagedComponents");
		},
		create: function(params) {
			return create(this.options, params, "packagedComponent");
		}
	}

	var TradingPartnerComponent = function(options) {
		this.options = copyOptions(options);
		this.options.object = "TradingPartnerComponent";
	}

	TradingPartnerComponent.prototype = {
		get: function(params) {
			return get(this.options, params, "tradingPartnerComponent");
		},
		query: function(params) {
			return query(this.options, params, "tradingPartnerComponents");
		},
		create: function(params) {
			return create(this.options, params, "tradingPartnerComponent");
		},
		update: function(params) {
			return update(this.options, params, "tradingPartnerComponent");
		},
		del: function(params) {
			return del(this.options, params);
		}
	}

	var APIUsageCount = function(options) {
		this.options = copyOptions(options);
		this.options.object = "APIUsageCount";
	}

	APIUsageCount.prototype = {
		query: function(params) {
			return query(this.options, params, "apiUsageCounts");
		}
	}

	BoomiAPI.account = function(options) {
		return new Account(options);
	};

	BoomiAPI.accountGroupAccount = function(options) {
		return new AccountGroupAccount(options);
	};

	BoomiAPI.accountGroupUserRole = function(options) {
		return new AccountGroupUserRole(options);
	};

	BoomiAPI.accountGroup = function(options) {
		return new AccountGroup(options);
	};

	BoomiAPI.accountSSOConfig = function(options) {
		return new AccountSSOConfig(options);
	};

	BoomiAPI.accountUserFederation = function(options) {
		return new AccountUserFederation(options);
	};

	BoomiAPI.accountUserRole = function(options) {
		return new AccountUserRole(options);
	};

	BoomiAPI.atomConnectionFieldExtensionSummary = function(options) {
		return new AtomConnectionFieldExtensionSummary(options);
	};

	BoomiAPI.atomConnectorVersions = function(options) {
		return new AtomConnectorVersions(options);
	};

	BoomiAPI.environment = function(options) {
		return new Environment(options);
	};

	BoomiAPI.integrationPack = function(options) {
		return new IntegrationPack(options);
	};

	BoomiAPI.integrationPackInstance = function(options) {
		return new IntegrationPackInstance(options);
	};

	BoomiAPI.integrationPackEnvironmentAttachment = function(options) {
		return new IntegrationPackEnvironmentAttachment(options);
	};

	BoomiAPI.cloud = function(options) {
		return new Cloud(options);
	};

	BoomiAPI.atom = function(options) {
		return new Atom(options);
	};

	BoomiAPI.environmentAtomAttachment = function(options) {
		return new EnvironmentAtomAttachment(options);
	};

	BoomiAPI.environmentExtensions = function(options) {
		return new EnvironmentExtensions(options);
	};

	BoomiAPI.atomExtensions = function(options) {
		return new AtomExtensions(options);
	};

	BoomiAPI.process = function(options) {
		return new Process(options);
	};

	BoomiAPI.executionRecord = function(options) {
		return new ExecutionRecord(options);
	};

	BoomiAPI.processSchedules = function(options) {
		return new ProcessSchedules(options);
	};

	BoomiAPI.accountUserRole = function(options) {
		return new AccountUserRole(options);
	};

	BoomiAPI.environmentMapExtensionsSummary = function(options) {
		return new EnvironmentMapExtensionsSummary(options);
	};

	BoomiAPI.atomMapExtensionsSummary = function(options) {
		return new AtomMapExtensionsSummary(options);
	};

	BoomiAPI.environmentMapExtension = function(options) {
		return new EnvironmentMapExtension(options);
	};

	BoomiAPI.atomMapExtension = function(options) {
		return new AtomMapExtension(options);
	};

	BoomiAPI.event = function(options) {
		return new Event(options);
	};

	BoomiAPI.executionCountAccount = function(options) {
		return new ExecutionCountAccount(options);
	};

	BoomiAPI.executionCountAccountGroup = function(options) {
		return new ExecutionCountAccountGroup(options);
	};

	BoomiAPI.executionSummaryRecord = function(options) {
		return new ExecutionSummaryRecord(options);
	};

	BoomiAPI.integrationPackAtomAttachment = function(options) {
		return new IntegrationPackAtomAttachment(options);
	};

	BoomiAPI.atomStartupProperties = function(options) {
		return new AtomStartupProperties(options);
	};

	BoomiAPI.auditLog = function(options) {
		return new AuditLog(options);
	};

	BoomiAPI.customTrackedField = function(options) {
		return new CustomTrackedField(options);
	};

	BoomiAPI.deployment = function(options) {
		return new Deployment(options);
	};

	BoomiAPI.documentCountAccount = function(options) {
		return new DocumentCountAccount(options);
	};

	BoomiAPI.documentCountAccountGroup = function(options) {
		return new DocumentCountAccountGroup(options);
	};

	BoomiAPI.environmentConnectionFieldExtensionSummary = function(options) {
		return new EnvironmentConnectionFieldExtensionSummary(options);
	};

	BoomiAPI.processAtomAttachment = function(options) {
		return new ProcessAtomAttachment(options);
	};

	BoomiAPI.processEnvironmentAttachment = function(options) {
		return new ProcessEnvironmentAttachment(options);
	};

	BoomiAPI.processScheduleStatus = function(options) {
		return new ProcessScheduleStatus(options);
	};

	BoomiAPI.role = function(options) {
		return new Role(options);
	};

	BoomiAPI.sharedServerInformation = function(options) {
		return new SharedServerInformation(options);
	};

	BoomiAPI.throughputAccount = function(options) {
		return new ThroughputAccount(options);
	};

	BoomiAPI.throughputAccountGroup = function(options) {
		return new ThroughputAccountGroup(options);
	};

	BoomiAPI.widgetInstanceExtensions = function(options) {
		return new WidgetInstanceExtensions(options);
	};

	BoomiAPI.x12ConnectorRecord = function(options) {
		return new X12ConnectorRecord(options);
	};

	BoomiAPI.accountProvision = function(options) {
		return new AccountProvision(options);
	};

	BoomiAPI.installerToken = function(options) {
		return new InstallerToken(options);
	};

	BoomiAPI.environmentRole = function(options) {
		return new EnvironmentRole(options);
	};

	BoomiAPI.componentAtomAttachment = function(options) {
		return new ComponentAtomAttachment(options);
	};

	BoomiAPI.componentEnvironmentAttachment = function(options) {
		return new ComponentEnvironmentAttachment(options);
	};

	BoomiAPI.listenerStatus = function(options) {
		return new ListenerStatus(options);
	};

	BoomiAPI.packagedComponent = function(options) {
		return new PackagedComponent(options);
	};

	BoomiAPI.tradingPartnerComponent = function(options) {
		return new TradingPartnerComponent(options);
	};

	BoomiAPI.apiUsageCount = function(options) {
		return new APIUsageCount(options);
	};

	BoomiAPI.getAssignableRoles = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "getAssignableRoles";
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "GET",
            success: function(results, deferred) {
            	params.roles = results.Role;
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.executeProcess = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "executeProcess";
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "POST",
            data: params.data,
            noResponse: true,
            success: function(results, deferred) {
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.cancelExecution = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "cancelExecution";
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "POST",
            id: params.executionId,
            success: function(results, deferred) {
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.deployProcess = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "deployProcess";
		var additional = "/"+params.environmentId+"/"+params.digest;
		if (params.listenerStatus) {
			if (params.overrideAccountId) {
				additional += "?overrideAccount="+params.overrideAccountId;
				additional += "&listenerStatus="+params.listenerStatus;
			} else {
				additional += "?listenerStatus="+params.listenerStatus;
			}
			params.overrideAccountId = undefined;
		}
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "POST",
            id: params.deploymentId,
            additional: additional,
            success: function(results, deferred) {
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.deployComponent = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "deployComponent";
		var additional = "/"+params.environmentId+"/"+params.digest;
		if (params.listenerStatus) {
			if (params.overrideAccountId) {
				additional += "?overrideAccount="+params.overrideAccountId;
				additional += "&listenerStatus="+params.listenerStatus;
			} else {
				additional += "?listenerStatus="+params.listenerStatus;
			}
			params.overrideAccountId = undefined;
		}
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "POST",
            id: params.deploymentId,
            additional: additional,
            success: function(results, deferred) {
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.changeListenerStatus = function(options, params) {
		var opts = copyOptions(options);
		opts.object = "changeListenerStatus";
		return callApi(opts, {
			overrideAccountId: params.overrideAccountId,
            type: "POST",
            data: params.data,
            success: function(results, deferred) {
                deferred.resolve(params);
			}
		});
	};

	BoomiAPI.createProxy = function(options) {
		if (BoomiAPIProxy) {
			return BoomiAPIProxy.createProxy(options);
		} else {
			return null;
		}
	}

	return BoomiAPI;
}));
