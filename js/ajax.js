// Copyright (c) 2016 Dell Boomi, Inc.

var http = require("http");
var https = require("https");
var url = require('url');

function isObject(obj) {
  return obj === Object(obj);
}

function ajax(options) {
	var parsedUrl = url.parse(options.url);
	//console.log("debug: parsedUrl ["+JSON.stringify(parsedUrl)+"]");
	
	var useSSL = parsedUrl.protocol === "https:";
	var handler = useSSL ? https : http;
	var port = Number(parsedUrl.port) || (useSSL ? 443 : 80);
	
	var reqopts = {
    	host: parsedUrl.hostname,
    	path: parsedUrl.pathname,
    	method: options.type,
    	port: port,
    	headers: options.headers,
    	rejectUnauthorized: options.rejectUnauthorized
	}
	if (parsedUrl.search) {
		reqopts.path += parsedUrl.search;
	}
	//console.log("debug: reqopts ["+JSON.stringify(reqopts)+"]");
	var jqXHR = {
	    readyState: 0,
		status: 0,
		statusText: "success"
	};
	
	var request = handler.request(reqopts, function(response) {
		jqXHR.status = response.statusCode;
	
		var respdata = '';
		response.on("data", function(data) {
			respdata += data;
		});
	
		response.on("end", function() {
			var data = respdata;
			jqXHR.responseText = data;
			
			if (options.dataType && options.dataType === 'json') {
				try {
					data = JSON.parse(respdata.replace(/[\cA-\cZ]/gi, ''));
				} catch (e) {
					options.error(jqXHR, "error", e);
				}
			}
		
			var success = jqXHR.status >= 200 && jqXHR.status < 300 || jqXHR.status === 304;
		
			if (jqXHR.status === 204 || options.type === 'HEAD') {
				jqXHR.statusText = 'nocontent'
			} else if (jqXHR.status === 304) {
				jqXHR.statusText = 'notmodified'
			}
		
			jqXHR.readyState = jqXHR.status > 0 ? 4 : 0;
		
			if (success) {
				options.success(data, jqXHR.statusText, jqXHR);
			} else {
				var errmsg = http.STATUS_CODES[jqXHR.status];
				if (isObject(data) && data.message) {
					errmsg += " : "+data.message;
				}
				jqXHR.statusText = 'error';
				jqXHR.responseText = errmsg;
				options.error(jqXHR, "error", new Error(errmsg));
			}
		});
	});
	
	request.on("error", function(err) {
		jqXHR.status = 500;
		jqXHR.statusText = 'error';
		jqXHR.responseText = err.message;
		options.error(jqXHR, "error", err);	
	});

  	if (options.type !== 'GET' && options.data) {
  		request.write(options.data, 'utf-8');
  	}	
	request.end();
}

module.exports = ajax;