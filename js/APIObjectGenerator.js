// Copyright (c) 2018 Dell Boomi, Inc.

(function(factory) {
	var root = (typeof self == 'object' && self.self == self && self) ||
      		   (typeof global == 'object' && global.global == global && global);
	// AMD
	if (typeof define === "function" && define.amd) {
		define(["SwaggerParser", "q", "exports"], function(SwaggerParser, Q, exports) {
			root.APIObjectGenerator = factory(root, exports, Q.Promise, SwaggerParser, "./swagger.json");
		});
	// Node.js or CommonJS	
	} else if (typeof exports !== 'undefined') {
		var SwaggerParser = require("swagger-parser");
		factory(root, exports, Promise, SwaggerParser, "./swagger.json");
	// Browser global
	} else {
		root.APIObjectGenerator = factory(root, {},root.Q.Promise, root.SwaggerParser, "./swagger.json");
	}
}(function(root, APIObjectGenerator, PromiseImpl, SwaggerParser, swaggerFilePath) {


var definitions;

function generate(schema) {
	var obj = {};
	for (var key in schema.properties) {
		var property = schema.properties[key];
		if (property && property.type) {
			switch (property.type) {
				case "string":
					if (property.enum) {
						obj[key] = property.enum[0];
					} else {
						obj[key] = "";
					}	
					break;
				case "number":
				case "integer":
					obj[key] = 0;
					break;
				case "boolean":
					obj[key] = false;
					break;
				case "object":
					obj[key] = generate(property, definitions);
					break;
				case "array":
					obj[key] = [];
					if (property.items && property.items["$ref"]) {
						var schemaPath = property.items["$ref"].substring("#definitions/".length+1);
						var arrayschema = definitions[schemaPath];
						if (arrayschema) {
							var arrayItem = generate(arrayschema);
							obj[key][0] = arrayItem;
						}	
					}
					break;		
			}
		} else if (property && property["$ref"]) {
			var schemaPath = property["$ref"].substring("#definitions/".length+1);
			var s = definitions[schemaPath];
			if (s) {
				obj[key] = generate(s);
			}	
		}
	}
	return obj;
}

function loadDefinitions(cb) {
	SwaggerParser.parse(swaggerFilePath)
	.then(function(api) {
		definitions = api.definitions;
		cb();
	});
}

APIObjectGenerator.getAPIObjectTypes = function() {
	function getTypes() {
		var types = [];
		for (var key in definitions) {
			types.push(key);
		}
		return types;
	}
	var promise = new PromiseImpl(function(resolve, reject) {
		if (definitions) {
			resolve(getTypes());
		} else {
			loadDefinitions(() => {
				resolve(getTypes());
			});
		}
	});
	return promise;
}

APIObjectGenerator.generateAPIObject = function(type) {
	function gen() {
		if (definitions[type]) {
			return generate(definitions[type]);
		} else {
			return null;
		}	
	}
	var promise = new PromiseImpl(function(resolve, reject) {
		if (definitions) {
			var generated = gen();
			if (generated) {
				resolve(generated);
			} else {
				reject(type + " not found");
			}	
		} else {
			loadDefinitions(() => {
				var generated = gen();
				if (generated) {
					resolve(generated);
				} else {
					reject(type + " not found");
				}
			});	
		}
	});
	return promise;
}

return APIObjectGenerator;

}));
