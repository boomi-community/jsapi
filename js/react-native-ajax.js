// Copyright (c) 2019 Dell Boomi, Inc.

function isObject(obj) {
  return obj === Object(obj);
}

function ajax(options) {
    let jqXHR = {
	    readyState: 0,
		status: 0,
		statusText: "success"
	};

    let requestOptions = {
        method: options.type,
        headers: options.headers,
    };
    if (options.type !== 'GET' && options.data) {
        requestOptions.body = options.data;
    }

    fetch(options.url, requestOptions)
    .then((response) => {
        jqXHR.status = response.status;

        let promise;

        if (options.dataType && options.dataType === 'json') {
            promise = response.json();
        } else {
            promise = response.text();
        }

        promise.then((data) => {
            let success = jqXHR.status >= 200 && jqXHR.status < 300 || jqXHR.status === 304;

			if (jqXHR.status === 204 || options.type === 'HEAD') {
				jqXHR.statusText = 'nocontent'
			} else if (jqXHR.status === 304) {
				jqXHR.statusText = 'notmodified'
			}

			jqXHR.readyState = jqXHR.status > 0 ? 4 : 0;

            if (success) {
                options.success(data, jqXHR.statusText, jqXHR);
            } else {
                var errmsg = "HTTP Error : "+jqXHR.status;
                if (isObject(data) && data.message) {
                    errmsg += " : "+data.message;
                }
                jqXHR.statusText = 'error';
                jqXHR.responseText = errmsg;
                options.error(jqXHR, "error", new Error(errmsg));
            }
        })
        .catch((err) => {
            options.error(jqXHR, "error", err);
        });
    })
    .catch((err) => {
        options.error(jqXHR, "error", err);
    });
}

module.exports = ajax;
