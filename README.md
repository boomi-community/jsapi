### Boomi JS API ###

The BoomiAPI.js module is the core module used to make the [Boomi API](http://help.boomi.com/atomsphere/GUID-C6847C47-5EFF-4933-ADA1-A47D032471C6.html) calls. It is designed to be usable in 3 different environments

* Server-side in [Node.js](https://nodejs.org/) using [CommonJS](http://wiki.commonjs.org/wiki/CommonJS) syntax
* Client-side as a browse global variable. Simply include the module with a script tag
* Client-side using [AMD](https://github.com/amdjs/amdjs-api/wiki/AMD). Use an AMD based loader such as [RequireJS](http://requirejs.org/) to load

The module has dependencies depending on the environment used.

When used server-side the dependencies are :

* [q](http://documentup.com/kriskowal/q/) Promise framework
* [btoa](https://www.npmjs.com/package/btoa) for Node.js

When used client-side the dependencies are :

* q Promise framework
* [jQuery](https://jquery.com/)

The module provides method to create API objects for each Boomi API Object type. You create an instance of an Object Type providing the necessary credentials and then call methods to perform the supported actions. The action methods will vary depending on the Object Type. Check the API documentation to find out which methods are available :

* ***get*** - requires an id property in the input object
```
#!javascript

var input = {
    "id": "..."
};
```
* ***query*** - requires a filter object or a queryToken property in the input object
```
#!javascript

var input = {
    "filter": {
        "QueryFilter" : {
            "expression" : {}
        }
    }
};
```
* ***create*** - requires a data object in the input object
```
#!javascript

var input = {
    "data": {}
};
```
* ***del*** - requires an id property in the input object
```
#!javascript

var input = {
    "id": "..."
};
```
* ***update*** - requires an id property and a data object in the input object
```
#!javascript

var input = {
    "id": "...",
    "data": {}
};
```
* ***execute*** - requires a data object in the input object
```
#!javascript

var input = {
    "data": {}
};
```
* ***bulk*** - requires a bulk object in the input object
```
#!javascript

var input = {
    "bulk": {
        "type" : "GET",
        "request" : []
    }
};
```
Additionally an ***overrideAccountId*** property can be added to the input for all action methods. This allows for Partner API calls to be made. See the [Boomi Partner API documentation](http://help.boomi.com/atomsphere/GUID-04F4AA2A-DACF-4278-B10A-5D3F645C0FA0.html) for more details.

Each action method returns a “q” deferred object that enables sequences of APIs to be chained together.

Here is an example creating an Account Object and calling query on it to obtain a list of all accounts :

```
#!javascript

var creds {
    baseUrl: "https://api.boomi.com",
    userid: "",
    password: "",
    accountId: "",
    debug: true
};

var account = BoomiAPI.account(creds);

var filter = {
    "QueryFilter" : {
        "expression" : {}
    }
};

account.query({filter: filter})
.then(function(results) {
    // result.accounts will contain the array of accounts    
})
.fail(function(error) {
    // handle failure
});

```

Here is an example that shows using the API to obtain all the accounts for a given Account Sub Group Id. Two API objects are created using a provided credentials object. The Account Group Account Object query method is called with a filter object providing the account group id. A bulk API call to the Account object using the return set of account ids provides each of the associated Account details.

```
#!javascript

// Create the credentials object
var creds {
    baseUrl: "https://api.boomi.com",
    userid: "",
    password: "",
    accountId: "",
    debug: true
};

// Create the filter object for the accountGroupAccount query
var filter = {
    "QueryFilter" : {
        "expression" : {
            "operator" : "EQUALS",
            "property" : "accountGroupId",
            "argument" : [“<accountGroupId>”]
        }
    }
};

// Use the BoomiAPI object to create an Account Group Account Object
var accountGroupAccount = BoomiAPI.accountGroupAccount(creds);
// Use the BoomiAPI object to create an Account Object
var account = BoomiAPI.account(creds);

// Call the query method of Account Group Account object using the required filter

accountGroupAccount.query({filter: filter})
.then(function(results) {
    var bulk = {
        "type" : "GET",
        "request" : []
    };
    // The results object will contain a accountGroupAccounts that has the list of Account Group Accounts
    // Add each account id found to the bulk object
    results.accountGroupAccounts.result.forEach(function(accountGroupAccount) {
        bulk.request.push({id: accountGroupAccount.accountId});
    });
    // Clear the filter from the results object
    results.filter = undefined;
    // Set the bulk object
    results.bulk = bulk;
    // Specify this is a partner call
    results.partner = true;
    // Call the bulk method of the Account object returning the promise back to the chain
    // Use the existing results object as input
    return account.bulk(results);
})
.then(function(results) {
    // Setup a data object containing all the accounts objects to be returned
    var data = {
        accounts: []
    };
    results.accounts.response.forEach(function(response) {
        // If the status is ok and the account has not been deleted add it to the accounts list
        if (response.statusCode === 200 && response.Result.status !== "deleted") {
            data.accounts.push(response.Result);
        }
    });

    //data object contains the accounts
})
.fail(function(error) {
    // handle failure
});
```
#### AngularJS Usage ####
You must use global browse object mode and ensure the required dependencies, ***JQuery*** and ***q*** are loaded via script tag.
```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/q.js/1.4.1/q.min.js"></script>
```
and also a script tag for the BoomiAPI.js file itself
```
<script src="../../BoomiAPI.js"></script>
```
Here is an example service that gets Atoms

```
var creds {
    baseUrl: "https://api.boomi.com",
    userid: "",
    password: "",
    accountId: "",
    debug: true
};

var filter = {
    "QueryFilter" : {
        "expression" : {}
    }
};

app.service('atomsService', function () {
	this.getAtoms = function(cb) {
		var atom = BoomiAPI.atom(creds);

		atom.query({filter: filter})
		.then(function(results) {
			cb(null, results.atoms.result);
		})
		.fail(function(error) {
			cb(error);
		});
	}
});
```
In your controller that uses the service make sure to use ***$scope.$apply()*** to apply the changes

```
app.controller('AtomsController', function ($scope, atomsService) {
	$scope.atoms = [];
	atomsService.getAtoms(function(err, atoms) {
		$scope.$apply(function() {
			$scope.atoms = atoms;
		});
	});
});
```

#### React Usage ####

Although React provides a CommonJS/Node.js like environment it does not provide everything that BoomiAPI requires. Because of this the BoomiAPI module must be imported as follows :

```
import BoomiAPI from 'BoomiAPI/dist/react/BoomiAPI';
```


#### Proxy Server Setup & Usage ####

If you do not want to embed your boomi credentials in code being run browser side then it is possible to configure a proxy that run in a server-side node.js application. This example
uses the "express" module to create and support the proxy.

```

const express = require('express');
const app = express();
const auth = require('basic-auth');
const BoomiAPI = require('BoomiAPI');

let credentials = {
	baseUrl: "https://api.boomi.com",
	userid: "...",
	password: "...",
	accountId: "....",
	debug: true,
	isProxy: true
};

// Save off userid, password and baseUrl
const boomiUserid = credentials.userid;
const boomiPassword = credentials.password;
const baseUrl = credentials.baseUrl;

// Null them out if credentials object is loadable by browser based code
credentials.userid = undefined;
credentials.password = undefined;
credentials.baseUrl = "";

// Create proxy using saved userid, password and baseUrl
const proxy = BoomiAPI.createProxy({baseUrl: baseUrl, userid: boomiUserid, password: boomiPassword});

// Do your own authentication here. This example checks for a user of "admin" and a password of 'xxxxx'

function authenticate(req, res) {
	let credentials = auth(req);

  	if (!credentials || credentials.name !== 'admin' || credentials.pass !== 'xxxxx') {
    	res.statusCode = 401;
    	res.setHeader('WWW-Authenticate', 'Basic realm="Boomi IoT Demo"');
    	res.end('Access denied');
    	return false;
  	} else {
  		return true;
	}
}

// Specify which Boomi API objects are valid
const validAPIObjects = ["ProcessEnvironmentAttachment", "Process", "ProcessSchedules", "executeProcess"];

// Catch all incoming Boomi API calls and validate them by checking account id and which API objects are supported
app.all("/api/rest/v1/*", function(req, res) {
	if (authenticate(req, res)) {
		let path = req.url;
		if (path.charAt(0) == '/') {
			path = path.substring(1);
		}
		var segments = path.split("/");
		let valid = true;
		let accountId = segments[3];
		let apiObject = segments[4];
		if (accountId !== credentials.accountId) {
			valid = false;
		}
		if (validAPIObjects.indexOf(apiObject) === -1) {
			valid = false;
		}
		if (valid) {
			proxy.web(req, res);
		} else {
			res.statusCode = 401;
			res.end('Access denied');
		}
	}
});
```

On browser side

```
// Make sure the "isProxy" property is set true.
var creds = {
	accountId: "....",
	debug: true,
	isProxy: true
};

var account = BoomiAPI.account(creds);

......
```

#### Creating template Boomi API objects from swagger file ####

If you have a need to create template/default Boomi API data objects you can use the APIObjectGenerator module.
It contains 2 methods

* getAPIObjectTypes	-	returns an array of all the available types
* generateAPIObject	-	generates an object of the type specified

Both methods return Promise objects. You use them as follows

```
APIObjectGenerator.getAPIObjectTypes().then(function(types) {
	types.forEach(function(type) {
		.....
	});
});


APIObjectGenerator.generateAPIObject("Atom").then(function(obj) {
	.....
}).catch(function(err) {
	.....
});					

```

As with the BoomAPI module you can load the APIObjectGenerator module in the same 3 ways

* Server-side in [Node.js](https://nodejs.org/) using [CommonJS](http://wiki.commonjs.org/wiki/CommonJS) syntax
* Client-side as a browse global variable. Simply include the module with a script tag
* Client-side using [AMD](https://github.com/amdjs/amdjs-api/wiki/AMD). Use an AMD based loader such as [RequireJS](http://requirejs.org/) to load

The module depends on :

* [swagger-parser](https://www.npmjs.com/package/swagger-parser) Swagger Parser
